﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{

    public class Circle
    {    // private instance variable, not accessible from outside this class
        private double radius;
        private String color;

        // The default constructor with no argument.
        // It sets the radius and color to their default value.
        public Circle()
        {
            radius = 1.0;
            color = "red";
        }

        // 2nd constructor with given radius, but color default
        public Circle(double r)
        {
            radius = r;
            color = "red";
        }

        // A public method for retrieving the radius
        public double getRadius()
        {
            return radius;
        }

        public void setRadius(double radius)
        {
            this.radius = radius;
        }


        // A public method for computing the area of circle
        public double getArea()
        {
            return radius * radius * Math.PI;
        }
        public override string ToString()

        {
            return radius + " " + color;
        }
        public double getCircumference()
        {
            return 2 * Math.PI * radius;
        }
    }

 }


