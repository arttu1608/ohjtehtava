﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class InvoiceItem
    {
        private string id;
        private string desc;
        private int qty;
        private double unitPrice;

        public InvoiceItem(string id, string desc, int qty, double unitPrice) { }
        public string getID()
        {
            return id;
        }
        public string getDesc()
        {
            return desc;
        }
        public int getQty()
        {
            return qty;
        }
        public void setQty(int qty)
        {
            this.qty = qty;
        }
        public double getUnitPrice()
        {
            return unitPrice;
        }
        public void setUnitPrice(double unitPrice)
        {
            this.unitPrice = unitPrice;
        }
        /* public double getTotal( )
         {
            selvitä ennen kun palautat!!
         }*/
        public String toString()
        {
            return "InvoiceItem[id= " + id + ", desc= "+ desc +" ,unitPrice= " + unitPrice;
        }
    }
}
