﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class Employee
    {
        private int id;
        private string firstname;
        private string lastname;
        private int salary;

        public Employee (int id, string firstname, string lastname, int salary) {
            this.id = id;
            this.firstname = firstname;
            this.lastname = lastname;
            this.salary = salary;
        }

        public int getID()
        {
            return id;
        }
        public string getFirstname()
        {
            return firstname;
        }
        public string getLastname()
        {
            return lastname;
        }
        public string getName()
        {
            return firstname + " " + lastname;
        }
        public int getSalary()
        {
            return salary;
        }
        public void setSalary(int salary)
        {
            this.salary = salary;
        }
        public int AnnualSalary()
        {
            return salary * 12;
        }
        public int raiseSalary(int percent)
        {
            salary = salary + percent;
            return salary;
        }
        public String toString()
        {
            return "Employee[id= " + id + ", name= " + firstname+ lastname+" ,salary= "+salary;
        }






    }
}
